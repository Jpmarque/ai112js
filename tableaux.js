let t = ["a", "b", "c", "d"];

let tab = [];

tab.push(1); // ajout à la fin
tab.push(2);
tab.push(3);
tab.push(4);

tab.unshift(-1); // ajout au début

console.log(tab);

let x = tab[0]; // lecture sans modifier le tableau

x = tab.pop(); // lecture et retrait du denrier élément

console.log(x);
console.log(tab);

x = tab.shift(); // lecture et retrait du premier element

console.log(x);
console.log(tab);
