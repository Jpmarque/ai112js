        // ajoute a coté de chaque titre un lien vers la recherche google associée

        // exemple : 
        // source html : <h2>benzema</h2>
        // après transformation : <h2>benzema <a href="https://www.google.com/search?q=benzema">X</a></h2>

        //https://www.google.com/search?q=intro

        // récupérer la liste des h1 et des h2 de la page
        let titles = document.querySelectorAll("h1, h2");

        // pour chaque titre
        for (let i = 0; i < titles.length; i++) {
            // je récupère le ieme élément
            let title = titles[i];

            // faire la modif
            title.innerHTML = title.innerText
                + "<a target='_new' href='https://translate.google.com/?sl=fr&tl=en&text=" + title.innerText + "'>X</a>";
        }