
// déclaration d'une fonction :
function additionner(a, b) {
    return a + b;
}

function multiplier(a, b) {
    return a * b;
}

function effectuerOperation(a, b, operation) {
    var resultat = operation(a,b);
    console.log("Le resultat de " + a + " et " + b + " est égal à " + resultat);
}

effectuerOperation(39,3, additionner);
effectuerOperation(21,2, multiplier);

effectuerOperation(2,3, (a,b) => {
        return (b*7) * a;
    });


// appel de fonction : 
var resultat = additionner(39,3);

//console.log(resultat);

function lancerD6() {
    //var jet = Math.round(Math.random() * 5) + 1;

    var jet = Math.random(); // valeur réelle entre 0 et 1
    jet = jet * 5; // valeur réelle entre 0 et 5
    jet = Math.round(jet); // valeur entière entre 0 et 5
    jet++; // valeur entière entre 1 et 6

    return jet;
}

//for (var i=0; i<50; i++) {
    var res = lancerD6();
    console.log(res);
//}


var truc = lancerD6; // alias sur une fonction 
console.log(truc());







