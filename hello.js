"use strict"; // mode strict

//console.log("Hello World");

// déclaration
var x;
// affectation
x = 42;

// declaration + affectation
var nom = "plop";

// Number
var n = 42;

// String
var s = "plop";

// Boolean
var b = true;

//console.log(n);

n = "bonjour";

//console.log(n);

s = 4654.5;

var t = null;
//console.log(t);

// Not a Number : NaN

var a = 42;
var b = "42";

console.log(a+Number(b)); // conversion str to number: Number()

if(isNaN(b)){
    console.log("b n'est pas un nombre");
}

// === : égalité de valeur ET de type
if(b === 42){
    console.log("super");
}


var txtAge =  "lkfjlds";
var age;

if(isNan(txtAge)) {
    age = 0;
}
else {
    age = Number(txtAge);
}

















